﻿namespace MVCShowsTV.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ppaltables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chapters",
                c => new
                    {
                        ChapterId = c.Int(nullable: false, identity: true),
                        ChapterName = c.String(),
                        Duration = c.String(),
                        SeasonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ChapterId)
                .ForeignKey("dbo.Seasons", t => t.SeasonId, cascadeDelete: true)
                .Index(t => t.SeasonId);
            
            CreateTable(
                "dbo.Seasons",
                c => new
                    {
                        SeasonId = c.Int(nullable: false, identity: true),
                        SeasonName = c.String(),
                        ShowId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SeasonId)
                .ForeignKey("dbo.Shows", t => t.ShowId, cascadeDelete: true)
                .Index(t => t.ShowId);
            
            CreateTable(
                "dbo.Shows",
                c => new
                    {
                        ShowId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ShowId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Chapters", "SeasonId", "dbo.Seasons");
            DropForeignKey("dbo.Seasons", "ShowId", "dbo.Shows");
            DropIndex("dbo.Seasons", new[] { "ShowId" });
            DropIndex("dbo.Chapters", new[] { "SeasonId" });
            DropTable("dbo.Shows");
            DropTable("dbo.Seasons");
            DropTable("dbo.Chapters");
        }
    }
}
