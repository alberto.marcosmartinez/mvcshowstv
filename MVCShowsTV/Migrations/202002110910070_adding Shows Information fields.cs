﻿namespace MVCShowsTV.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingShowsInformationfields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shows", "Country", c => c.String());
            AddColumn("dbo.Shows", "ReleaseYear", c => c.String());
            AddColumn("dbo.Shows", "OnBroadCast", c => c.Boolean(nullable: false));
            AddColumn("dbo.Shows", "Finished", c => c.Boolean(nullable: false));
            AddColumn("dbo.Shows", "Cancelled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shows", "Cancelled");
            DropColumn("dbo.Shows", "Finished");
            DropColumn("dbo.Shows", "OnBroadCast");
            DropColumn("dbo.Shows", "ReleaseYear");
            DropColumn("dbo.Shows", "Country");
        }
    }
}
