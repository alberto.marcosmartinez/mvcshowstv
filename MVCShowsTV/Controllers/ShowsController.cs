﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCShowsTV.Models;
using PagedList;

namespace MVCShowsTV.Controllers
{
    public class ShowsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Shows
        public ActionResult Index(string sortOrder,string currentFilter,string searchString, bool? chkCancelled, bool? chkBroadcast, bool? chkEnded ,int? page)
        {
            ViewBag.TitleSortParam = String.IsNullOrEmpty(sortOrder) ? "title_desc" : "title";
            ViewBag.YearSortParam = String.IsNullOrEmpty(sortOrder) ? "year_desc" : "year";
            ViewBag.CancelledParam = chkCancelled;
            ViewBag.Broadcast = chkBroadcast;
            ViewBag.Ended = chkEnded;
            ViewBag.Cancelled = chkCancelled;
            ViewBag.CurrentSort = sortOrder;

            if (searchString != null)
            {
                page = 1;
            }
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            ApplicationDbContext context = new ApplicationDbContext();
            IQueryable<Show> shows = context.Shows;

            if (!String.IsNullOrEmpty(searchString))
                shows = shows.Where(s => s.Title.Contains(searchString));
            
            if (chkCancelled == true)
                shows = shows.Where(x => x.Cancelled == true);

            if (chkBroadcast == true)
                shows = shows.Where(x => x.OnBroadCast == true);

            if (chkEnded == true)
                shows = shows.Where(x => x.Finished == true);


            switch (sortOrder)
            {
                case "title_desc":
                    shows = shows.OrderByDescending(s => s.Title);
                    break;
                case "title":
                    shows = shows.OrderBy(s => s.Title);
                    break;
                case "year_desc":
                    shows = shows.OrderByDescending(s => s.ReleaseYear);
                    break;
                case "year":
                    shows = shows.OrderBy(s => s.ReleaseYear);
                    break;
                default:
                    shows = shows.OrderBy(s => s.ReleaseYear);
                    break;
            }            

            ViewBag.Series = shows;

            var list = shows.Select(s => new { s.ShowId, s.Title }).ToList();


            if (Request.UrlReferrer != null)
                ViewBag.UrlVolver = Request.UrlReferrer.ToString();

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(shows.ToPagedList(pageNumber, pageSize));
        }

        // GET: Shows/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Show show = db.Shows.Find(id);
            if (show == null)
            {
                return HttpNotFound();
            }
            return View(show);
        }

        // GET: Shows/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shows/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShowId,Title")] Show show)
        {
            if (ModelState.IsValid)
            {
                db.Shows.Add(show);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(show);
        }

        // GET: Shows/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Show show = db.Shows.Find(id);
            if (show == null)
            {
                return HttpNotFound();
            }
            return View(show);
        }

        // POST: Shows/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShowId,Title")] Show show)
        {
            if (ModelState.IsValid)
            {
                db.Entry(show).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(show);
        }

        // GET: Shows/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Show show = db.Shows.Find(id);
            if (show == null)
            {
                return HttpNotFound();
            }
            return View(show);
        }

        // POST: Shows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Show show = db.Shows.Find(id);
            db.Shows.Remove(show);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult removeSerie(String id, String idSerie)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            Show show = context.Shows.Where(x => x.ShowId.ToString().Equals(id)).FirstOrDefault();
            if (show != null)
            {               
                context.Shows.Remove(show);
                context.SaveChanges();
                
            }
            return RedirectToAction("Shows", "Index");
        }
    }
}
