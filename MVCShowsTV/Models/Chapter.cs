﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCShowsTV.Models
{
    public class Chapter
    {
        public int ChapterId { get; set; }

        public string ChapterName { get; set; }

        public string Duration { get; set; }

        public int SeasonId { get; set; }

        public virtual Season Season { get; set; }
    }
}