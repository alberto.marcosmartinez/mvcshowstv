﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCShowsTV.Models
{
    public class Season
    {
        public int SeasonId { get; set; }

        public string SeasonName { get; set; }

        public int ShowId { get; set; }

        public virtual Show Show { get; set; }
    }
}