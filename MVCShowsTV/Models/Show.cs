﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCShowsTV.Models
{
    public class Show
    {
        public int ShowId { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
        public string ReleaseYear { get; set; }
        public bool OnBroadCast { get; set; }
        public bool Finished { get; set; }
        public bool Cancelled { get; set; }
    }

}